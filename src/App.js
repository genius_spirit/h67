import React, { Component } from 'react';
import DoorLock from './components/DoorLock/DoorLock';
import Homepage from "./components/Homepage/Homepage";
import {Route, Switch} from "react-router-dom";
import Calculator from "./components/Calculator/Calculator";
import Nav from "./components/Nav/Nav";

class App extends Component {
  render() {
    return (
      <div style={{margin: "0 auto", textAlign: "center", maxWidth: "920px"}}>
        <Nav/>
        <Switch>
          <Route path="/" exact component={Homepage}/>
          <Route path="/doorLock" component={DoorLock}/>
          <Route path="/calculator" component={Calculator}/>
        </Switch>
      </div>
    );
  }
}

export default App;
