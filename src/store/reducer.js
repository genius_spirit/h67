
const initialState = {
  total: '',
  access: false,
  error: false,
  calc: ''
};
const accessCode = '5923';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'NUMBER':
      return { total: state.total + action.amount };
    case 'REMOVE':
      return { total: state.total.slice(0, state.total.length - 1) };
    case 'CHECK':
      if (state.total === accessCode) {
        return {access: true, total: ''}
      } else return {error: true, total: ''};
    case 'CALC':
      try {
        return {...state, calc: eval(state.total)};
      } catch(e) {
        return {calc: "wrong operation", total: ''};
      }
    case 'ZERO':
      return {total: ''};
    default:
      return state;
  }
};

export default reducer;