import React from 'react';
import './Nav.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
  return(
    <div className="Nav">
      <ul>
        <li className="Nav-link"><NavLink to="/">Homepage</NavLink></li>
        <li className="Nav-link"><NavLink to="/doorLock">Doorlock</NavLink></li>
        <li className="Nav-link"><NavLink to="/calculator">Calculator</NavLink></li>
      </ul>
    </div>
  )
};

export default Nav;