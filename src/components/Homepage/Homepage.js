import React from 'react';
import './Homepage.css';
import react from '../img/react.jpg';

const Homepage = () => {
  return(
    <div className="Homepage-container">
      <img src={react} alt="React js"/>
    </div>
  )
};

export default Homepage;