import React, { Component } from 'react';
import {connect} from "react-redux";
import './Calculator.css';

const keyboard = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0', '(', ')', '.'];
const calculating = ['+', '-', '*', '/'];

class Calculator extends Component {
  render() {
    return(
       <div className="Calc-container">
         <div className="Calc-screen">
           <div className="Calc-screen__example">{this.props.total}</div>
           <div className="Calc-screen__total">{this.props.calc}</div>
         </div>
         <div className="Calc-keyboard">
           <div className="Calc-keyboard__numbers">
             {keyboard.map((item, index) => {
               return <span key={index} className="Calc-keyboard-button" onClick={() => this.props.sendItem(item)}>{item}</span>
             })}
             <span className="Calc-keyboard-button" onClick={this.props.removeLastValue}>{'CE'}</span>
             <span className="Calc-keyboard-button primary-btn" onClick={this.props.calculate}>{'='}</span>
           </div>
           <div className="Calc-keyboard__calculating">
             <span className="Calc-keyboard-button" onClick={this.props.getZero}>{'AC'}</span>
             {calculating.map((item, index) => {
               return <span key={index} className="Calc-keyboard-button" onClick={() => this.props.sendItem(item)}>{item}</span>
             })}
           </div>
         </div>
       </div>
    )
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => {
  return {
    sendItem: (item) => dispatch({type: 'NUMBER', amount: item}),
    removeLastValue: () => dispatch({type: 'REMOVE'}),
    calculate: () => dispatch({type: 'CALC'}),
    getZero: () => dispatch({type: 'ZERO'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);